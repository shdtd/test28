<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('brand_id');
            $table->unsignedBigInteger('model_id');
            /*
                An integer from -128 to 127, counting since 2000.
                ----------------------------------------------------
                Число от -128 до 127 от 2000 года.
            */
            $table->tinyInteger('year')->default(0);
            $table->unsignedMediumInteger('mileage')->default(0);
            /*
                The RGB color can be stored as a VARCHAR or MEDIUMINT.
                * I choose a MEDIUMINT.
                ----------------------------------------------------
                Цвет можно записать как VARCHAR или как MEDIUMINT.
                * Я использую MEDIUMINT.
            */
            $table->unsignedMediumInteger('color')->default(0);
            $table->timestamps();
            /*
                Set cascadeOnDelete() if the data in the cars table is not important.
                The data will be deleted when the user is deleted.
                ----------------------------------------------------
                Если при удалении пользователя данные о машинах можно удалить
                установите cascadeOnDelete(), если нет, restrictOnDelete().
            */
            // Users
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            // Brands
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            // Models
            $table->foreign('model_id')
                ->references('id')
                ->on('models')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
