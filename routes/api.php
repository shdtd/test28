<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\ModelsController;
use App\Http\Controllers\CarsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

/**
 * Login and Register
 * ---------------------
 * Регистрация и авторизация
 */
Route::post('/auth/register', [AuthController::class, 'createUser']);
Route::post('/auth/login', [AuthController::class, 'loginUser']);
/**
 * Main application controllers API
 * ---------------------------------
 * API контроллеров приложения
 */
Route::middleware('auth:sanctum')->apiResource('/brands', BrandsController::class);
Route::middleware('auth:sanctum')->apiResource('/models', ModelsController::class);
Route::middleware('auth:sanctum')->apiResource('/cars', CarsController::class);
/**
 * User personal data
 * ---------------------------------
 * Данные пользователя
 */
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
