<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ModelsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        if ($this->user()->id > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        switch ($this->getMethod()) {
            case 'POST':
                return  [
                    'brand_id' => ['required', 'numeric'],
                    'model'    => [
                        'required',
                        'string',
                        Rule::unique('models', 'model')
                            ->where('brand_id', $this->input('brand_id')),
                    ],
                ];
            case 'PUT':
                return [
                    'id' => ['required', 'numeric', 'exists:models,id'],
                    'brand_id' => ['required', 'numeric'],
                    'model'    => [
                        'required',
                        'string',
                        Rule::unique('models', 'model')
                            ->where('brand_id', $this->input('brand_id'))
                            ->ignore($this->input('model'), 'model'),
                    ],
                ];
            case 'DELETE':
                return [
                    'id' => ['required', 'numeric', 'exists:models,id'],
                ];
            default:
                return [];
        }
    }

    /**
     * Make error messages for validation rules.
     * --------------------
     * Сообщения об ошибках при проверке данных.
     */
    public function messages()
    {
        return [
            'model.unique' => 'This model of selected brand already exists.',
        ];
    }

    /**
     * Add parameters from URL
     * ------------------------
     * Добавим параметры из URL
     */
    public function all($keys = null)
    {
      $data = parent::all($keys);
      $data['id'] = $this->route('model');
      return $data;
    }
}
