<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        if ($this->user()->id > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'brand' => ['required', 'string', 'unique:brands,brand'],
        ];

        switch ($this->getMethod()) {
            case 'POST':
                return $rules;
            case 'PUT':
                return [
                    'id' => ['required', 'numeric', 'exists:brands,id'],
                ] + $rules;
            case 'DELETE':
                return [
                    'id' => ['required', 'numeric', 'exists:brands,id'],
                ];
            default:
                return [];
        }
    }

    /**
     * Add parameters from URL
     * ------------------------
     * Добавим параметры из URL
     */
    public function all($keys = null)
    {
      $data = parent::all($keys);
      $data['id'] = $this->route('brand');
      return $data;
    }
}
