<?php

namespace App\Http\Controllers;

use App\Models\Models;
use App\Http\Requests\ModelsRequest;

class ModelsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Models::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ModelsRequest $request)
    {
        $model = Models::create($request->validated());
        return $model;
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $model = Models::find($id);
        return $model;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ModelsRequest $request, int $id)
    {
        $model = Models::find($id);

        if (empty($model) === true) {
            return response(null, 404);
        }

        $model->fill($request->except(['model_id']));
        $model->save();
        return response()->json($model);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $model = Models::find($id);

        if(empty($model) === false && $model->delete()) {
            return response(null, 204);
        }

        return response(null, 404);
    }
}
