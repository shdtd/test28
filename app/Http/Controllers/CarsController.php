<?php

namespace App\Http\Controllers;

use App\Models\Cars;
use App\Http\Requests\CarsRequest;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(CarsRequest $request)
    {
        return Cars::all()->where('user_id', $request->user()->id);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CarsRequest $request)
    {
        $userid = ['user_id' => $request->user()->id];
        $car = Cars::create($request->validated() + $userid);
        return $car;
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id, CarsRequest $request)
    {
        $car = Cars::where('user_id', $request->user()->id)->find($id);
        return $car;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CarsRequest $request, int $id)
    {
        $car = Cars::where('user_id', $request->user()->id)->find($id);

        if (empty($car) === true) {
            return response(null, 404);
        }

        $car->fill($request->except(['car_id']));
        $car->save();
        return response()->json($car);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id, CarsRequest $request)
    {
        $car = Cars::where('user_id', $request->user()->id)->find($id);

        if(empty($car) === false && $car->delete()) {
            return response(null, 204);
        }

        return response(null, 404);
    }
}
