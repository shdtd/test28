<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use App\Http\Requests\BrandsRequest;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Brands::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BrandsRequest $request)
    {
        $brand = Brands::create($request->validated());
        return $brand;
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $brand = Brands::find($id);
        return $brand;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BrandsRequest $request, int $id)
    {
        $brand = Brands::find($id);

        if (empty($brand) === true) {
            return response(null, 404);
        }

        $brand->fill($request->except(['brand_id']));
        $brand->save();
        return response()->json($brand);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $brand = Brands::findOrFail($id);

        if(empty($brand) === false && $brand->delete()) {
            return response(null, 204);
        }

        return response(null, 404);
    }
}
