<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    use HasFactory;

    protected $table    = 'cars';
    protected $fillable = ['user_id', 'brand_id', 'model_id', 'year', 'mileage', 'color'];
    protected $hidden   = ['created_at', 'updated_at'];
}
